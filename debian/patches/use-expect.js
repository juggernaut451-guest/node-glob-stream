--- a/test/index.js
+++ b/test/index.js
@@ -1,6 +1,6 @@
 'use strict';
 
-var expect = require('expect');
+var expect = require('expect.js');
 var miss = require('mississippi');
 
 var globStream = require('../');
@@ -25,8 +25,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -43,8 +43,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -61,8 +61,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -79,8 +79,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -99,8 +99,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -117,8 +117,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -147,7 +147,7 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(3);
+      expect(pathObjs.length).to.eql(3);
       expect(pathObjs).toInclude(expected[0]);
       expect(pathObjs).toInclude(expected[1]);
       expect(pathObjs).toInclude(expected[2]);
@@ -172,7 +172,7 @@
     });
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(2);
+      expect(pathObjs.length).to.eql(2);
     }
 
     pipe([
@@ -210,8 +210,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(3);
-      expect(pathObjs).toEqual(expected);
+      expect(pathObjs.length).to.eql(3);
+      expect(pathObjs).to.eql(expected);
     }
 
     pipe([
@@ -248,8 +248,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(3);
-      expect(pathObjs).toEqual(expected);
+      expect(pathObjs.length).to.eql(3);
+      expect(pathObjs).to.eql(expected);
     }
 
     pipe([
@@ -295,8 +295,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(5);
-      expect(pathObjs).toEqual(expected);
+      expect(pathObjs.length).to.eql(5);
+      expect(pathObjs).to.eql(expected);
     }
 
     pipe([
@@ -331,8 +331,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(3);
-      expect(pathObjs).toEqual(expected);
+      expect(pathObjs.length).to.eql(3);
+      expect(pathObjs).to.eql(expected);
     }
 
     pipe([
@@ -349,8 +349,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -367,8 +367,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -396,7 +396,7 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(2);
+      expect(pathObjs.length).to.eql(2);
       expect(pathObjs).toInclude(expected[0]);
       expect(pathObjs).toInclude(expected[1]);
     }
@@ -409,7 +409,7 @@
 
   it('ignores dotfiles without dot option', function(done) {
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(0);
+      expect(pathObjs.length).to.eql(0);
     }
 
     pipe([
@@ -426,8 +426,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -438,7 +438,7 @@
 
   it('removes dotfiles that match negative globs with dot option', function(done) {
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(0);
+      expect(pathObjs.length).to.eql(0);
     }
 
     pipe([
@@ -455,8 +455,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     var stream = globStream('./fixtures/test.coffee', { cwd: dir });
@@ -480,8 +480,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -503,8 +503,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -526,8 +526,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -538,7 +538,7 @@
 
   it('does not error when a negative glob removes all matches from a positive glob', function(done) {
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(0);
+      expect(pathObjs.length).to.eql(0);
     }
 
     pipe([
@@ -561,8 +561,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -584,8 +584,8 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -595,20 +595,20 @@
   });
 
   it('throws on invalid glob argument', function(done) {
-    expect(globStream.bind(globStream, 42, { cwd: dir })).toThrow(/Invalid glob .* 0/);
-    expect(globStream.bind(globStream, ['.', 42], { cwd: dir })).toThrow(/Invalid glob .* 1/);
+    expect(globStream.bind(globStream, 42, { cwd: dir })).to.throwException(/Invalid glob .* 0/);
+    expect(globStream.bind(globStream, ['.', 42], { cwd: dir })).to.throwException(/Invalid glob .* 1/);
     done();
   });
 
   it('throws on missing positive glob', function(done) {
-    expect(globStream.bind(globStream, '!c', { cwd: dir })).toThrow(/Missing positive glob/);
-    expect(globStream.bind(globStream, ['!a', '!b'], { cwd: dir })).toThrow(/Missing positive glob/);
+    expect(globStream.bind(globStream, '!c', { cwd: dir })).to.throwException(/Missing positive glob/);
+    expect(globStream.bind(globStream, ['!a', '!b'], { cwd: dir })).to.throwException(/Missing positive glob/);
     done();
   });
 
   it('emits an error when file not found on singular path', function(done) {
     function assert(err) {
-      expect(err).toMatch(/File not found with singular glob/);
+      expect(err).to.match(/File not found with singular glob/);
       done();
     }
 
@@ -644,7 +644,7 @@
 
   it('emits an error when a singular path in multiple paths not found', function(done) {
     function assert(err) {
-      expect(err).toMatch(/File not found with singular glob/);
+      expect(err).to.match(/File not found with singular glob/);
       done();
     }
 
@@ -656,7 +656,7 @@
 
   it('emits an error when a singular path in multiple paths/globs not found', function(done) {
     function assert(err) {
-      expect(err).toMatch(/File not found with singular glob/);
+      expect(err).to.match(/File not found with singular glob/);
       done();
     }
 
@@ -674,8 +674,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(1);
-      expect(pathObjs[0]).toEqual(expected);
+      expect(pathObjs.length).to.eql(1);
+      expect(pathObjs[0]).to.eql(expected);
     }
 
     pipe([
@@ -699,8 +699,8 @@
     var opts = {};
 
     var stream = globStream(dir + '/fixtures/stuff/run.dmc', opts);
-    expect(Object.keys(opts).length).toEqual(0);
-    expect(opts).toNotEqual(defaultedOpts);
+    expect(Object.keys(opts).length).to.eql(0);
+    expect(opts).to.not.eql(defaultedOpts);
 
     pipe([
       stream,
@@ -729,7 +729,7 @@
 
     it('does not have any effect on our results', function(done) {
       function assert(pathObjs) {
-        expect(pathObjs.length).toEqual(0);
+        expect(pathObjs.length).to.eql(0);
       }
 
       pipe([
@@ -749,8 +749,8 @@
       };
 
       function assert(pathObjs) {
-        expect(pathObjs.length).toEqual(1);
-        expect(pathObjs[0]).toEqual(expected);
+        expect(pathObjs.length).to.eql(1);
+        expect(pathObjs[0]).to.eql(expected);
       }
 
       pipe([
@@ -767,8 +767,8 @@
       };
 
       function assert(pathObjs) {
-        expect(pathObjs.length).toEqual(1);
-        expect(pathObjs[0]).toEqual(expected);
+        expect(pathObjs.length).to.eql(1);
+        expect(pathObjs[0]).to.eql(expected);
       }
 
       pipe([
@@ -779,7 +779,7 @@
 
     it('supports the ignore option with dot option', function(done) {
       function assert(pathObjs) {
-        expect(pathObjs.length).toEqual(0);
+        expect(pathObjs.length).to.eql(0);
       }
 
       pipe([
@@ -795,7 +795,7 @@
       ];
 
       function assert(pathObjs) {
-        expect(pathObjs.length).toEqual(0);
+        expect(pathObjs.length).to.eql(0);
       }
 
       pipe([
--- a/test/readable.js
+++ b/test/readable.js
@@ -1,6 +1,6 @@
 'use strict';
 
-var expect = require('expect');
+var expect = require('expect.js');
 var miss = require('mississippi');
 
 var stream = require('../readable');
@@ -22,7 +22,7 @@
 
   it('emits an error if there are no matches', function(done) {
     function assert(err) {
-      expect(err.message).toMatch(/^File not found with singular glob/g);
+      expect(err.message).to.match(/^File not found with singular glob/g);
       done();
     }
 
@@ -55,8 +55,8 @@
     gs.push(stub);
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(2);
-      expect(pathObjs[0]).toEqual(stub);
+      expect(pathObjs.length).to.eql(2);
+      expect(pathObjs[0]).to.eql(stub);
     }
 
     pipe([
@@ -73,8 +73,8 @@
     };
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toBe(1);
-      expect(pathObjs[0]).toMatch(expected);
+      expect(pathObjs.length).to.be(1);
+      expect(pathObjs[0]).to.match(expected);
     }
 
     pipe([
@@ -103,7 +103,7 @@
     ];
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toBe(3);
+      expect(pathObjs.length).to.be(3);
       expect(pathObjs).toInclude(expected[0]);
       expect(pathObjs).toInclude(expected[1]);
       expect(pathObjs).toInclude(expected[2]);
@@ -127,8 +127,8 @@
     }
 
     function assert(pathObjs) {
-      expect(pathObjs.length).toEqual(3);
-      expect(spy.calls.length).toEqual(2);
+      expect(pathObjs.length).to.eql(3);
+      expect(spy.calls.length).to.eql(2);
       spy.restore();
     }
 
@@ -147,7 +147,7 @@
     function assert(err) {
       spy.restore();
       expect(spy).toHaveBeenCalledWith(err);
-      expect(err).toMatch(/File not found with singular glob/);
+      expect(err).to.match(/File not found with singular glob/);
       done();
     }
 
@@ -173,7 +173,7 @@
       fsStub.restore();
       spy.restore();
       expect(spy).toHaveBeenCalledWith(err);
-      expect(err).toBe(expectedError);
+      expect(err).to.be(expectedError);
       done();
     }
 
